
package matteprogram;


import java.util.Random;
import java.util.Scanner;


public class Matteprogram {

    int userScore = 0;
    
    //Välkomst meddelande samt refvariabel till metoden tableselector
    
    public static void main(String[] args) {
    
    System.out.println("Välkommen till Multiplikationstabellen");
    Matteprogram ref = new Matteprogram();
    
    ref.tableSelector();
    
    } 
    public void tableSelector(){
        
    
    
    System.out.println("Välj vilken tabell du vill träna på genom att skriva en siffra");
    System.out.println(                     "1, 2, 3, 4, 5");
    System.out.println("==============================================================");
    
    Matteprogram ref = new Matteprogram();
    Scanner scan = new Scanner(System.in);
    
    int tabelAnswer = scan.nextInt();
    
    if(tabelAnswer==1){
        ref.tabellOne();
        
    }
    else if(tabelAnswer==2){
        ref.tabellTwo();
    }
    else if(tabelAnswer==3){
        ref.tabellThree();
    }
    else if(tabelAnswer==4){
        ref.tabellFour();
    }
    else if(tabelAnswer==5){
        ref.tabellFive();
    }
    else{
        System.out.println("error");
    }
   
    
     }    
 // Loop för tabell 1
    public void tabellOne() {
    
    System.out.println("Du valde tabell 1.");    
    System.out.println("Nu kommer 5st tal som du ska räkna ut, du fyller i svaret med siffror");
    System.out.println("När du besvarat alla 5 frågor ser du ditt resultat");
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    for(int i = 1; i <= 5; i++){
        
    int t1 = dice.nextInt(11);    
    
    System.out.println("Fråga " + i +": 1 * "+t1);
    
    int answer = scan.nextInt();
    
    
    if(t1*1 == answer){
    userScore++;
    }
    else{
        
    }
    System.out.println("Du fick "+userScore + " av 5 rätt");
    }
    
    
    //Slut på första tabellen
    System.out.println("Vill du spela igen tryck (1) Om du vill avsluta tryck (2)");
    int replay = scan.nextInt();
    
    if(replay==1){
        tableSelector();
    }
    else if(replay==2){
        return;
    }
    }
  
    
    
    
  // Loop för tabell 2
    public void tabellTwo() {
        
    System.out.println("Du valde tabell 2.");    
    System.out.println("Nu kommer 5st tal som du ska räkna ut, du fyller i svaret med siffror");
    System.out.println("När du besvarat alla 5 frågor ser du ditt resultat");    
        
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    for(int i = 1; i <= 5; i++){
        
    int t2 = dice.nextInt(11);    
    
    System.out.println("Fråga " + i +": 2 * "+t2);
    
    int answer = scan.nextInt();
    
    
    if(t2*2 == answer){
    userScore++;
    }
    else{
        
    }
    
    }
    System.out.println("Du fick "+userScore + " av 5 rätt");
    
    //Slut på andra tabellen
    System.out.println("Vill du spela igen tryck (1) Om du vill avsluta tryck (2)");
    int replay = scan.nextInt();
    
    if(replay==1){
        tableSelector();
    }
    else if(replay==2){
        return;
    }
    }
    // Loop för tabell 3
       public void tabellThree() {
           
    System.out.println("Du valde tabell 3.");    
    System.out.println("Nu kommer 5st tal som du ska räkna ut, du fyller i svaret med siffror");
    System.out.println("När du besvarat alla 5 frågor ser du ditt resultat");       
        
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    for(int i = 1; i <= 5; i++){
        
    int t3 = dice.nextInt(11);    
    
    System.out.println("Fråga " + i +": 3 * "+t3);
    
    int answer = scan.nextInt();
    
    
    if(t3*3 == answer){
    userScore++;
    }
    else{
        
    }
    
    }
    System.out.println("Du fick "+userScore + " av 5 rätt");
    
    //Slut på tredje tabellen
    System.out.println("Vill du spela igen tryck (1) Om du vill avsluta tryck (2)");
    int replay = scan.nextInt();
    
    if(replay==1){
        tableSelector();
    }
    else if(replay==2){
        return;
    }
    }
    // Loop för tabell 4
          public void tabellFour() {
              
    System.out.println("Du valde tabell 4.");    
    System.out.println("Nu kommer 5st tal som du ska räkna ut, du fyller i svaret med siffror");
    System.out.println("När du besvarat alla 5 frågor ser du ditt resultat");          
        
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    for(int i = 1; i <= 5; i++){
        
    int t4 = dice.nextInt(11);    
    
    System.out.println("Fråga " + i +": 4 * "+t4);
    
    int answer = scan.nextInt();
    
    
    if(t4*4 == answer){
    userScore++;
    }
    else{
        
    }
    
    }
    System.out.println("Du fick "+userScore + " av 5 rätt");
    
    //Slut på fjärde tabellen
    System.out.println("Vill du spela igen tryck (1) Om du vill avsluta tryck (2)");
    int replay = scan.nextInt();
    
    if(replay==1){
        tableSelector();
    }
    else if(replay==2){
        return;
    }
    }
    // Loop för tabell 5
    public void tabellFive() {
        
    System.out.println("Du valde tabell 5.");    
    System.out.println("Nu kommer 5st tal som du ska räkna ut, du fyller i svaret med siffror");
    System.out.println("När du besvarat alla 5 frågor ser du ditt resultat");    
        
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    for(int i = 1; i <= 5; i++){
        
    int t5 = dice.nextInt(11);    
    
    System.out.println("Fråga " + i +": 5 * "+t5);
    
    int answer = scan.nextInt();
    
    
    if(t5*5 == answer){
    userScore++;
    }
    else{
        
    }
    
    }
    System.out.println("Du fick "+userScore + " av 5 rätt");
    
    //Slut på femte tabellen
    System.out.println("Vill du spela igen tryck (1) Om du vill avsluta tryck (2)");
    int replay = scan.nextInt();
    
    if(replay==1){
        tableSelector();
    }
    else if(replay==2){
        return;
    }
    }
}

